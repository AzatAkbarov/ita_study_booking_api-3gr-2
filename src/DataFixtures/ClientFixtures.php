<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Model\Client\ClientHandler;
use Doctrine\Common\Persistence\ObjectManager;

class ClientFixtures extends Fixture
{
    /**
     * @var ClientHandler
     */
    private $clientHandler;

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    public function load(ObjectManager $manager)
    {

        $client1 = $this->clientHandler->createNewClient([
            'email' => 'asd@asd.asd',
            'passport' => 'AN12345678',
            'password' => '213471118',
        ]);

        $manager->persist($client1);

        $client2 = $this->clientHandler->createNewClient([
            'email' => 'qwe@qwe.qwe',
            'passport' => 'AN87654321',
            'password' => '1123581321',
        ]);

        $manager->persist($client2);

        $client3 = $this->clientHandler->createNewClient([
            'email' => 'zxc@zxc.zxc',
            'passport' => 'AN12344321',
            'password' => '32571219',
        ]);

        $manager->persist($client3);

        $client4 = $this->clientHandler->createNewClient([
            'email' => 'ewq@ewq.ewq',
            'passport' => 'AN123',
            'password' => '123',
        ]);

        $manager->persist($client4);

        $manager->flush();
    }

}